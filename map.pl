farbe(rot).
farbe(blau).
farbe(gelb).
farbe(gruen).

nachbar(Farbe1,Farbe2) :- Farbe1 \= Farbe2.

map(A,B,C,D,E) :-
  farbe(A),
  farbe(B),
  farbe(C),
  farbe(D),
  farbe(E),
  nachbar(A,B),
  nachbar(A,D),
  nachbar(A,E),
  nachbar(B,E),
  nachbar(B,C),
  nachbar(B,D),
  nachbar(C,E),
  nachbar(C,D),
  nachbar(D,E).

solutions(A,B,C,D,E,List) :- findall([A,B,C,D,E],map(A,B,C,D,E),List).

countSolutions(A,B,C,D,E,Count) :- solutions(A,B,C,D,E,List), length(List,Count).

go :- write("# of Solutions:"), countSolutions(A,B,C,D,E,Count), write(Count), nl,nl,write("Solutions:"), solutions(A,B,C,D,E,List), write(List).
