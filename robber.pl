/*
• Mr. Jones ist dick und fährt einen VW.
• Mr. Smith fährt einen Jaguar und ist Engländer.
• Der dünne Mann fährt einen Fiat.
• Mr. Poole ist Schotte.
• Mr. Kent trägt einen blauen Anzug.
• Der dicke Mann trägt einen schwarzen Anzug.
• Der dünne Mann ist Ire.
• Mr. Bright trägt einen grünen Anzug und fährt einen Renault.
• Der kleine Mann ist Schotte.
• Der dünne Mann trägt einen blauen Anzug.
• Der Mann normaler Statur ist Kanadier.
• Der Mann im grauen Anzug fährt einen Mazda.
• Der Mann im schwarzen Anzug ist Waliser.
• Der große Mann trägt einen braunen Anzug.
• Letzte Nacht war der Waliser mit dem Kanadier zusammen.
• Letzte Nacht war der große Mann mit dem Mazdafahrer zusammen.
*/

personen(X) :- X = [_,_,_,_,_],
  member([jones,_,dick,_,vw],X),
  member([smith,englaender,_,_,jaguar],X),
  member([_,_,duenn,_,fiat],X),
  member([poole,schotte,_,_,_],X),
  member([kent,_,_,blau,_],X),
  member([_,_,dick,schwarz,_],X),
  member([_,ire,duenn,_,_],X),
  member([bright,_,_,gruen,renault],X),
  member([_,schotte,klein,_,_],X),
  member([_,_,duenn,blau,_],X),
  member([_,kanadier,normal,_,_],X),
  member([_,_,_,grau,mazda],X),
  member([_,waliser,_,schwarz,_],X),
  member([_,_,gross,braun,_],X).

gesucht(L) :-
  personen(X),
  member([A,waliser,_,_,_],X),
  member([B,kanadier,_,_,_],X),
  member([C,_,gross,_,_],X),
  member([D,_,_,_,mazda],X),
  member([L,_,_,_,_],X),
  A \= B, C \=D, not(member(L,[A,B,C,D])).

go:- write("gesuchter Robber: "),gesucht(Robber),write(Robber).
