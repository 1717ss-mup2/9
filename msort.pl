/*
?- msort( [1, 2, 2, 1], X ).
X = [1, 1, 2, 2] .
*/

msort([],[]).
msort([A],[A]).
msort(E,A) :- split(E,E1,E2),msort(E1,A1), msort(E2,A2), merge(A1,A2,A).

split(L, L1, L2) :- append(L1, L2, L), mylength(L1, C), mylength(L2, C).

mylength([],0).
mylength([_|T],Cnew) :- mylength(T,Cold),Cnew is Cold+1.

merge([],L,L).
merge(L,[],L):- L\=[].
merge([H1|L1],[H2|L2],[H1|L]):- H1=<H2, merge(L1,[H2|L2],L).
merge([H1|L1],[H2|L2],[H2|L]):- H1>H2, merge([H1|L1],L2,L).

go:- write("msort([1,2,2,1],X) X="),msort([1,2,2,1],X),write(X).
